package controlador;
import Cotizacion.Cotizacion;
import Vista.dlgVista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener{
    
    private Cotizacion coti;
    private dlgVista vista;
    
    public Controlador(Cotizacion coti, dlgVista vista){
        this.coti= coti;
        this.vista = vista;
        
        //Hacer que el controlador escuche los botontes
        
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.cbPlazo.addActionListener(this);
    }
    
    public void iniciarVista(){
        vista.setTitle(":: COTIZACION ::");
        vista.setSize(680, 255);
        vista.setVisible(true);
    }
    
    public void limpiar(){
        vista.txtNumCoti.setText("");
        vista.txtDescripcion.setText("");
        vista.txtPrecio.setText("");
        vista.txtPorcentaje.setText("");
        vista.cbPlazo.setSelectedItem("select");
        vista.txtPagoIni.setText("");
        vista.txtTotalFin.setText("");
        vista.txtPagoMens.setText("");
    }

    public boolean isVacio(){
        if(this.vista.txtDescripcion.getText().equals("") ||this.vista.txtPrecio.getText().equals("") || this.vista.txtPorcentaje.getText().equals("")){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean isDescuentoValido(){
        if(this.vista.txtPorcentaje.getText().equals("100")){
            return false;
        }
        else{
            return true;
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        if(e.getSource()==vista.btnNuevo){
            vista.btnCancelar.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.cbPlazo.setEnabled(true);
            vista.txtDescripcion.enable(true);
            vista.txtPrecio.enable(true);
            vista.txtPorcentaje.enable(true);
            vista.txtNumCoti.setText(String.valueOf(coti.getNumCoti()));
        }
        
        else if(e.getSource()==vista.btnLimpiar){
            limpiar();
        }
       
        
        else if(e.getSource()==vista.btnCancelar){
            limpiar();
            vista.btnCancelar.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.cbPlazo.setEnabled(false);
            vista.txtNumCoti.enable(false);
            vista.txtDescripcion.enable(false);
            vista.txtPrecio.enable(false);
            vista.txtPorcentaje.enable(false);
        }
        
        else if(e.getSource()==vista.btnGuardar){
            if(isVacio()==true){
                JOptionPane.showMessageDialog(vista, " CAMPOS VACIOS ");
            }
            else{
                if(isDescuentoValido()==false){
                    JOptionPane.showMessageDialog(vista, "SELECCIONA DESCUENTO menor a 100");
                }
                else{
                    coti.setNumCoti();
                    coti.setDescripcion(vista.txtDescripcion.getText());
                    try{
                        coti.setPrecio(Integer.parseInt(vista.txtPrecio.getText()));
                        coti.setPorcentaje(Integer.parseInt(vista.txtPorcentaje.getText()));
                        coti.setPlazo(Integer.parseInt(vista.cbPlazo.getSelectedItem().toString()));
                        JOptionPane.showMessageDialog(vista, "Se agrego correctamente");
                        limpiar(); 
                    }
                    catch(NumberFormatException ex){
                        JOptionPane.showMessageDialog(vista, "Aparecio el siguiente error: "+ ex.getMessage());
                    }
                    
                }
            }
        }
        
        
        else if(e.getSource()==vista.btnMostrar){
            vista.txtNumCoti.setText(String.valueOf(coti.getNumCoti()));
            vista.txtDescripcion.setText(coti.getDescripcion());
            vista.txtPrecio.setText(String.valueOf(coti.getPrecio()));
            vista.txtPorcentaje.setText(String.valueOf(coti.getPorcentaje()));
            vista.cbPlazo.setSelectedItem(Integer.toString(coti.getPlazo()));
            vista.txtPagoIni.setText(Integer.toString(coti.calcularPagoIni()));
            vista.txtTotalFin.setText(Integer.toString(coti.calcularPagoRes()));
            vista.txtPagoMens.setText(Integer.toString(coti.calcularPagoMen()));
        }
        
        
        else if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista, "¿Quiere salir del programa?",
                    "Decide",JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
    }
    
    
    public static void main(String[] args) {
        Cotizacion coti = new Cotizacion();
        dlgVista vista = new dlgVista(new JFrame(), true);
        Controlador control = new Controlador (coti, vista);
        control.iniciarVista();
    }
}